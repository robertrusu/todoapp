/// Shortcuts
const text = document.getElementById("text");
const addTask = document.getElementById("addNewTask");
const saveTask = document.getElementById("savebtn");
const list = document.getElementById("list");
const saveIndex = document.getElementById("saveIndex");
let todoArray = [];

/// trigger event cand apas pe Add, si trigger display task
addTask.addEventListener("click", (e) => {
e.preventDefault();
let todo = localStorage.getItem("todo");
        if (todo === null) {
                todoArray = [];
        } else {
                todoArray = JSON.parse(todo);
        }
    todoArray.push(text.value);
    text.value = "";
    localStorage.setItem("todo", JSON.stringify(todoArray));
    displayTodo();
});

/// pentru fiecare click ne apare un task nou 
function displayTodo(){
    let todo = localStorage.getItem("todo");
        if (todo === null) {
            todoArray = []
        } else{
            todoArray = JSON.parse(todo);
        }

let htmlC = "";

todoArray.forEach((list, ind) => {
        htmlC += `  </br>
                    <div class="field columns has-addons">
                        <div class="control column is-9">
                            <div class="content">
                            <p class="subtitle is-size-6 has-text-black">${list}</p>
                            </div>
                        </div></br>
                        <div class="control column auto">
                            <a onClick='succesTask(${ind})' class="button is-success">Done</a>
                            <a onClick='edit(${ind})' class="button is-primary">Editare</a>
                            <a onCLick='deleteTodo(${ind})'class="button is-danger show-modal">Ștergere</a>
                        </div>
                        
                    </div>`
});

list.innerHTML = htmlC;

}


/// Felicitari ai realizat un task este same ca deletetask
function succesTask(ind){
    let todo = localStorage.getItem("todo");
    todoArray = JSON.parse(todo);
    todoArray.splice(ind, 1);
    localStorage.setItem("todo", JSON.stringify(todoArray));
    displayTodo()

}

/// Stergem un task
function deleteTodo(ind){
    let todo = localStorage.getItem("todo");
    todoArray = JSON.parse(todo);
    todoArray.splice(ind, 1);
    localStorage.setItem("todo", JSON.stringify(todoArray));
    displayTodo()
}

/// Editare task 
function edit(ind) {
    saveIndex.value = ind;
    let todo = localStorage.getItem("todo");
    todoArray = JSON.parse(todo);
    text.value = todoArray[ind];
    addTask.style.display = "none";
    saveTask.style.display = "block";
}

/// Salvare dupa ce am editat un anumit task
saveTask.addEventListener("click", () => {
    let todo = localStorage.getItem("todo");
    todoArray = JSON.parse(todo);
    let id =saveIndex.value;
    todoArray[id] = text.value;
    addTask.style.display ="block";
    saveTask.style.display ="none";
    text.value = "";
    localStorage.setItem("todo",JSON.stringify(todoArray));
    displayTodo();
})